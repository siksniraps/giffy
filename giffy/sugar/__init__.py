import logging
log = logging.getLogger("giffy.sugar")

import datetime
import json
import types
from collections import OrderedDict

import uuid
import re

from passlib.hash import sha256_crypt

def random_image_name():
    return random_token()

def random_token():
    return str(uuid.uuid4())


def email_is_valid(email):
    return re.match("^.+@.+\..{2,3}$", email) != None


def gen_password(password):
    hash = sha256_crypt.encrypt(password)
    return hash

def check_password(password, enc_password):
    return sha256_crypt.verify(password, enc_password)


def str2int(s, d=0):
    try:
        s = int(s)
    except ValueError, e:
        s = d
    return s


def datetime2str(dt):
    return dt.strftime('%Y-%m-%dT%H:%M:%SZ') if dt else None


def date2str(d):
    return d.isoformat() if d else None


def jsonable(obj, include=None, exclude=None, filter_prefix=""):
    if hasattr(obj, "_asdict"):
        # namedtuple handling
        objdict = obj._asdict()
        if hasattr(obj, "hidden_fields") and obj.hidden_fields:
            [objdict.pop(k) for k in obj.hidden_fields if k in objdict]
        obj = jsonable(objdict)
    elif isinstance(obj, list) or isinstance(obj, types.GeneratorType):
        # lists
        obj = [jsonable(list_item) for list_item in obj]
    elif isinstance(obj, dict):
        # dicts
        obj = OrderedDict((k, jsonable(v)) for k, v in obj.iteritems() if v is not None)
    elif isinstance(obj, datetime.datetime):
        # datetime
        obj = datetime2str(obj)
    elif isinstance(obj, datetime.date):
        # date
        obj = date2str(obj)
    elif hasattr(obj, "__dict__"):
        # objects
        obj = jsonable(obj.__dict__)
    elif isinstance(obj, str):
        obj = unicode(obj, "utf-8")

    if include or exclude:
        obj = filter_jsonable(obj, include, exclude, filter_prefix)
    return obj


def filter_jsonable(obj, include=None, exclude=None, filter_prefix=""):

    def include_key(k):
        if include:
            if k not in include:
                return False
        if exclude:
            if k in exclude:
                return False
        return True

    def filter_values(prefix, v):
        if (include and prefix in include) or exclude:
            if isinstance(v, list):
                v = [filter_values(prefix, lv) for lv in v]
            elif isinstance(v, dict):
                v = OrderedDict(
                    (kk, filter_values(prefix + kk + ".", vv))
                    for kk, vv in v.items()
                    if include_key(prefix + kk)
                )
        return v

    return filter_values(filter_prefix, obj)


def json_dumps(obj, indent=4):
    return json.dumps(jsonable(obj), indent=indent, ensure_ascii=False)
