import logging
log = logging.getLogger("giffy.endpoints")

from tornado import web
from uuid import uuid4
import traceback
import json

import giffy
from giffy.dao.user import UserDAO
from giffy.dao.gif import GifDAO
from giffy.dao.like import LikeDAO
from giffy.dao.tag import TagDAO
from giffy.dao.follower import FollowerDAO
from giffy import sugar as s
from tornado import gen


class RequestHandler(web.RequestHandler):

    @property
    def db(self):
        return self.application.db

    @property
    def mc(self):
        return self.application.mc

    @property
    def user(self):
        return UserDAO(self.db)

    @property
    def gif(self):
        return GifDAO(self.db)

    @property
    def like(self):
        return LikeDAO(self.db)

    @property
    def follower(self):
        return FollowerDAO(self.db)

    @property
    def tag(self):
        return TagDAO(self.db)

    @property
    def config(self):
        return self.application.config

    def json_body(self):
        try:
            return json.loads(self.request.body)
        except:
            log.error("Invalid JSON payload: %s" % self.request.body)
            raise giffy.Error(giffy.Error.INVALID_JSON)

    def get_json_argument(self, key, default=None):
        body = self.json_body()
        try:
            return body.get(key, default)
        except:
            return default

    def _set_request_id_header(self):
        if "X-Giffy-RequestId" not in self._headers:
            self.request_id = str(uuid4())
            self.set_header("X-Giffy-RequestId", self.request_id)

    def extract_access_token(self):
        return self.request.headers.get("X-Giffy-AccessToken") or self.get_argument("accessToken", None)

    def extract_account_id(self):
        account_id = self.request.headers.get("X-Giffy-AccountId") or self.get_argument("accountId", None)
        if account_id is not None:
            return int(account_id)

    def prepare(self):
        self._set_request_id_header()

    def write_json(self, chunk):
        chunk = s.json_dumps(chunk)
        self.set_header("Content-Type", "application/json; charset=UTF-8")
        self.write(chunk)

    def write_error(self, status_code, **kwargs):
        exc_info = kwargs.get("exc_info", None)
        self._set_request_id_header()

        if not hasattr(self, "giffy_error_message"):
            # generic error
            self.giffy_error_code = 0
            self.giffy_error_message = "HTTPError %s" % status_code

        if exc_info and status_code == 500:
            self.set_header('Content-Type', 'text/plain')
            for line in traceback.format_exception(*kwargs["exc_info"]):
                self.write(line)
            self.finish()
            return

        elif exc_info:
            if hasattr(exc_info[1], "error_code"):
                self.giffy_error_code = exc_info[1].error_code
                self.giffy_error_message = exc_info[1].error_message
            elif hasattr(exc_info[1], "log_message"):
                self.giffy_error_code = 0
                self.giffy_error_message = "HTTPError %d: %s" % (status_code, exc_info[1].log_message)
        self.write_json({
            "error": self.giffy_error_code,
            "message": self.giffy_error_message,
            "status_code": status_code,
        })


class AuthRequestHandler(RequestHandler):

    def extract_access_token(self):
        return self.request.headers.get("X-Giffy-AccessToken") or self.get_argument("accessToken", None)

    def extract_account_id(self):
        account_id = self.request.headers.get("X-Giffy-AccountId") or self.get_argument("accountId", None)
        if account_id is not None:
            return int(account_id)

    @gen.coroutine
    def prepare(self):
        self._set_request_id_header()
        self.access_token = self.extract_access_token()
        self.account_id = self.extract_account_id()

        if self.access_token is None:
            raise giffy.Error(giffy.Error.MISSING_PARAMETERS, "Missing X-Giffy-AccessToken header or accessToken parameter")
        if self.account_id is None:
            raise giffy.Error(giffy.Error.MISSING_PARAMETERS, "Missing X-Giffy-AccountId header or accountId parameter")
        account_id = yield self.mc.get("access-token-" + self.access_token)
        if account_id is None or account_id != self.account_id:
            raise giffy.Error(giffy.Error.UNAUTHORIZED)

class ErrorHandler(RequestHandler):

    def initialize(self, error_code):
        self.error = giffy.Error(error_code)

    def prepare(self):
        raise self.error
