import logging

from tornado import gen
import momoko

class FollowerDAO(object):
    def __init__(self, db):
        self.db = db

    @gen.coroutine
    def get(self, user_id=0, follower_id=0):
        sql = """
            SELECT follower_id
            FROM followers
            WHERE user_id=%s AND follower_id=%s
        """
        cursor = yield self.db.execute(sql, (user_id, follower_id))
        desc = cursor.description
        result = [dict(zip([col[0] for col in desc], row))
                         for row in cursor.fetchall()]
        cursor.close()
        if len(result) == 0:
            result = None
        else:
            result = result[0]
        raise gen.Return(result)

    @gen.coroutine
    def get_followers_list(self, user_id=0):
        sql = """
            SELECT id, username, email
            FROM users where id IN(
                SELECT follower_id
                FROM followers
                WHERE user_id=%s)
        """
        cursor = yield self.db.execute(sql, (user_id,))
        desc = cursor.description
        result = [dict(zip([col[0] for col in desc], row))
                         for row in cursor.fetchall()]
        cursor.close()
        raise gen.Return(result)

    @gen.coroutine
    def get_following_list(self, follower_id=0):
        sql = """
            SELECT id, username, email
            FROM users where id IN(
                SELECT user_id
                FROM followers
                WHERE follower_id=%s)
        """
        cursor = yield self.db.execute(sql, (follower_id,))
        desc = cursor.description
        result = [dict(zip([col[0] for col in desc], row))
                         for row in cursor.fetchall()]
        cursor.close()
        raise gen.Return(result)

    @gen.coroutine
    def create(self, user_id, follower_id):
        sql = """
            INSERT INTO followers (user_id, follower_id)
            VALUES (%s, %s)
            RETURNING *;
        """
        cursor = yield self.db.execute(sql, (user_id, follower_id))
        row_id = cursor.fetchone()[0]
        cursor.close()
        raise gen.Return(row_id)

    @gen.coroutine
    def delete(self, user_id, follower_id):
        sql = """
            DELETE
            FROM followers
            WHERE user_id=%s AND follower_id=%s
        """
        cursor = yield self.db.execute(sql, (user_id, follower_id))
        cursor.close()
        raise gen.Return(cursor)
